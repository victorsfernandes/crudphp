<?php
/* Conexao */
include_once 'php/dbConnect.php';
/* Header */
include_once 'includes/header.php';
/* Mensagem */
include_once 'includes/mensagem.php';
/* Sessão */

?>

<div class="row" id="conteudo" style="display: none;">
    <div class="container">
        <div id="tabela">
            <div class="col s12 m12 l12">
                <h3 class="light">Cliente</h3>
                <table class="striped">
                    <thead>
                        <tr>
                            <th>ID: </th>
                            <th>Nome: </th>
                            <th>Email: </th>
                            <th>CPF: </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT * FROM cliente";
                        $resultado = mysqli_query($connect, $sql);

                        if (mysqli_num_rows($resultado) > 0) :


                            while ($dados = mysqli_fetch_array($resultado)) :
                                ?>
                                <tr>
                                    <td><?php echo $dados['id']; ?></td>
                                    <td><?php echo $dados['nome']; ?></td>
                                    <td><?php echo $dados['email']; ?></td>
                                    <td><?php echo $dados['cpf']; ?></td>
                                    <td>
                                        <a href="#modalEditar<?php echo $dados['id']; ?>" class="btn modal-trigger green accent-4"><i class="material-icons">create</i></a>

                                    </td>
                                    <td>
                                        <a href="#modalExcluir<?php echo $dados['id']; ?>" class="btn red darken-1 modal-trigger"><i class="material-icons">clear</i></a>
                                    </td>

                                    <!-- Modal Excluir -->
                                    <div id="modalExcluir<?php echo $dados['id']; ?>" class="modal">
                                        <div class="modal-content">

                                            <!-- Modal Conteudo -->
                                            <h4>Opss...</h4>
                                            <p>Tem certeza que deseja remover o cliente?</p>
                                        </div>
                                        <div class="modal-footer">

                                            <form action="php/deletar.php" method="POST">
                                                <input type="hidden" name="id" value="<?php echo $dados['id']; ?>">
                                                <button type="submit" name="btnDeletar" class="btn red">Sim</button>
                                                <a href="index.php" class="btn green">Cancelar</a>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- Modal Editar -->
                                    <div id="modalEditar<?php echo $dados['id']; ?>" class="modal">
                                        <div class="modal-content">
                                            <!-- Modal Conteudo -->

                                            <form action="php/update.php" method="POST">
                                                <div class="container">
                                                    <div class="input-field">
                                                        <input type="hidden" name="id" value="<?php echo $dados['id']; ?>">
                                                        <input type="text" name="nome" id="nome" value="<?php echo $dados['nome']; ?>">
                                                        <label for="nome">nome</label>
                                                    </div>
                                                    <div class="input-field">
                                                        <input type="text" name="email" id="email" value="<?php echo $dados['email']; ?>">
                                                        <label for="email">email</label>
                                                    </div>
                                                    <div class="input-field">
                                                        <input type="text" name="cpf" id="cpf" value="<?php echo $dados['cpf']; ?>">
                                                        <label for="cpf">cpf</label>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" href="php/update.php" class="btn" name="btnEditar">Atualizar</button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
            </div>
            </tr>
        <?php endwhile;
        else : ?>
        <tr>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
        </tr>
    <?php endif; ?>
    </tbody>
    </table>




    <div class="container" style="margin-top: 20px; align-content: center;">
        <!-- Botão para criar -->
        <!-- Modal Trigger -->
        <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i class="material-icons">add</i></a>

        <!-- Modal Structure -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <!-- Modal Conteudo -->
                <form action="php/adicionar.php" method="POST">
                    <div class="container">
                        <div class="input-field">
                            <input type="text" name="nome" id="nome" required>
                            <label for="nome">Nome:</label>
                        </div>
                        <div class="input-field">
                            <input type="text" name="email" id="email" required>
                            <label for="email">Email:</label>
                        </div>
                        <div class="input-field">
                            <input type="text" name="cpf" id="cpf" required>
                            <label for="cpf">CPF:</label>
                        </div>

                        <!-- Btn criar dentro do modal-->
                        <div class="modal-footer">
                            <button type="submit" class="btn" name="btnCadastrar">criar</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

        </div>


        <div class="divider">teste</div>


        <!-- aeronave -->
        <div class="col s12 m12 l12">
            <h3 class="light">Aeronave</h3>

            <table class="striped">
                <thead>
                    <tr>
                        <th>ID: </th>
                        <th>ID Cliente: </th>
                        <th>Matricula: </th>
                        <th>Modelo: </th>
                        <th>Ano: </th>
                        <th>Cor: </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sql = "SELECT * FROM aeronave";
                    $resultado = mysqli_query($connect, $sql);

                    if (mysqli_num_rows($resultado) > 0) :


                        while ($dados = mysqli_fetch_array($resultado)) :
                            ?>
                            <tr>
                                <td><?php echo $dados['id']; ?></td>
                                <td><?php echo $dados['id_cliente']; ?></td>
                                <td><?php echo $dados['matricula']; ?></td>
                                <td><?php echo $dados['modelo']; ?></td>
                                <td><?php echo $dados['ano']; ?></td>
                                <td><?php echo $dados['cor']; ?></td>
                                <td>
                                    <a href="#modalEditar2<?php echo $dados['id']; ?>" class="btn modal-trigger green accent-4"><i class="material-icons">create</i></a>


                                </td>
                                <td>
                                    <a href="#modalExcluir2<?php echo $dados['id']; ?>" class="btn red darken-1 modal-trigger"><i class="material-icons">clear</i></a>

                                    <div id="modalExcluir2<?php echo $dados['id']; ?>" class="modal">
                                        <div class="modal-content">

                                            <!-- Modal Conteudo -->
                                            <h4>Opss...</h4>
                                            <p>Tem certeza que deseja remover o cliente?</p>
                                        </div>
                                        <div class="modal-footer">

                                            <form action="php/deletarAeronav.php" method="POST">
                                                <input type="hidden" name="id" value="<?php echo $dados['id']; ?>">
                                                <button type="submit" name="btnDeletar2" class="btn red">Sim</button>
                                                <a href="index.php" class="btn green">Cancelar</a>
                                            </form>
                                        </div>
                                    </div>

                                </td>

                                <!-- Modal Excluir -->
                                <div id="modalExcluir<?php echo $dados['id']; ?>" class="modal">
                                    <div class="modal-content">

                                        <!-- Modal Conteudo -->
                                        <h4>Opss...</h4>
                                        <p>Tem certeza que deseja remover o cliente?</p>
                                    </div>
                                    <div class="modal-footer">

                                        <form action="php/deletar.php" method="POST">
                                            <input type="hidden" name="id" value="<?php echo $dados['id']; ?>">
                                            <button type="submit" name="btnDeletar" class="btn red">Sim</button>
                                            <a href="index.php" class="btn green">Cancelar</a>
                                        </form>
                                    </div>
                                </div>

                                <!-- Modal Editar -->

                                <div id="modalEditar2<?php echo $dados['id']; ?>" class="modal">
                                    <div class="modal-content">
                                        <form action="php/updateAeronav.php" method="POST">
                                            <div class="modal-content container">

                                                <!-- Modal Conteudo -->
                                                <div class="input-field col s6">
                                                    <input id="id_Cliente" type="text" class="validate" value="<?php echo $dados['id']; ?>">
                                                    <label for="id_Cliente">ID Cliente</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="Matricula" type="text" class="validate" value="<?php echo $dados['matricula']; ?>">
                                                    <label for="Matricula">Matricula</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="Modelo" type="text" class="validate" value="<?php echo $dados['modelo']; ?>">
                                                    <label for="Modelo">Modelo</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="Ano" type="text" class="validate" value="<?php echo $dados['ano']; ?>">
                                                    <label for="Ano">Ano</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="Cor" type="text" class="validate" value="<?php echo $dados['cor']; ?>">
                                                    <label for="Cor">Cor</label>
                                                </div>

                                            </div>
                                            <div class="modal-footer col s12">
                                                <button type="submit" class="btn" name="btnEditar2">criar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>


                            </tr>
                        <?php
                            endwhile;
                        else : ?>
                        <tr>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                    <?php
                    endif;
                    ?>

                </tbody>
            </table>

            <!-- criar aeronave -->
            <div class="container" style="margin-top: 20px; align-content: center;">
                <!-- Modal Trigger -->
                <a class="waves-effect waves-light btn modal-trigger" href="#modal2"><i class="material-icons">add</i></a>

                <!-- Modal Structure -->
                <div id="modal2" class="modal">
                    <form action="php/adicionarAeronav.php" method="POST">
                        <div class="modal-content container">

                            <!-- Modal Conteudo -->
                            <div class="input-field col s6">
                                <input id="id_Cliente" type="text" class="validate">
                                <label for="id_Cliente">ID Cliente</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="Matricula" type="text" class="validate">
                                <label for="Matricula">Matricula</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="Modelo" type="text" class="validate">
                                <label for="Modelo">Modelo</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="Ano" type="text" class="validate">
                                <label for="Ano">Ano</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="Cor" type="text" class="validate">
                                <label for="Cor">Cor</label>
                            </div>

                        </div>
                        <div class="modal-footer col s12">
                            <button type="submit" class="btn" name="btnCadastrar2">criar</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>
</div>

<!-- Login -->

<?php

if (isset($_POST['btnEntrar'])) :
    $erros = array();
    $login = mysqli_escape_string($connect, $_POST['login']);
    $senha = mysqli_escape_string($connect, $_POST['senha']);

    if (empty($login) or empty($senha)) :
        $erros[] = "<li>Já preencheu os campos de login e senha?</li>";
    else :
        $sql = "SELECT login FROM usuario WHERE login = '$login'";
        $resultado = mysqli_query($connect, $sql);

        if (mysqli_num_rows($resultado) > 0) :
            $senha = md5($senha);
            $sql = "SELECT * FROM usuario WHERE login = '$login' AND senha = '$senha'";
            $resultado = mysqli_query($connect, $sql);

            if (mysqli_num_rows($resultado) == 1) :
                /* logado */
                $dados = mysqli_fetch_array($resultado);
                $_SESSION['logado'] = true;
                $_SESSION['idUsuario'] = $dados['id'];


                if ($_SESSION['logado'] == true) :
                    echo "
                        <script>
                        document.getElementById('conteudo').style.display = 'block';
                        document.getElementById('contLogin').style.display = 'none';
                    </script>
                    ";
                else :
                    echo "
                        <script>
                        document.getElementById('conteudo').style.display = 'none';
                        document.getElementById('contLogin').style.display = 'block';
                    </script>
                    ";
                endif;
            else :
                $erros[] = "<li>Usuário e senha não conferem.</li>";
            endif;

        else :
            $erros[] = "<li>Usuario não existe.</li>";
        endif;
    endif;
endif;
?>


<div class="container" id="contLogin" style="display: block;">

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <div class="row center-align" id="login">
            <?php
            if (!empty($erros)) :
                foreach ($erros as $erro) :
                    echo $erro;
                endforeach;
            endif;
            ?>
            <div class="input-field col s6 offset-s3">
                <i class="material-icons prefix">person</i>
                <input id="icon_prefix2" class="materialize-textarea" name="login" type="text"></input>
                <label for="icon_prefix2">Login</label>
            </div>

            <div class="input-field col s6 offset-s3">
                <i class="material-icons prefix">security</i>
                <input id="icon_prefix3" class="materialize-textarea" name="senha" type="password"></input>
                <label for="icon_prefix3">Senha</label>
            </div>
            <div class="col s2 center-align">
                <button class="btn" id="btnEntrar" type="submit" name="btnEntrar">entrar</button>
            </div>
        </div>
    </form>
</div>

<?php
/* Footer */
include_once 'includes/footer.php';
?>