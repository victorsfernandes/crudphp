create database crud;
use crud;
CREATE TABLE `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` char(100) NOT NULL,
  `email` char(150) DEFAULT NULL,
  `cpf` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE TABLE `usuario` (
	`id` int NOT NULL auto_increment,
    `login` varchar(255),
    `senha` varchar(255),
    primary key (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

INSERT INTO usuario (login, senha)
VALUES ("admin", md5("admin"));

CREATE TABLE `aeronave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `matricula` char(6) NOT NULL,
  `modelo` char(50) NOT NULL,
  `ano` year(4) DEFAULT NULL,
  `cor` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `aeronave_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;