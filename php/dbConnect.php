<?php
    /* DB connect */

    $serverName = "localhost";
    $username = "root";
    $password = "";
    $dbName = "crud";
    $port = "3307";

    $connect = mysqli_connect($serverName, $username, $password, $dbName, $port);
    mysqli_set_charset($connect, "utf8");

    if (mysqli_connect_errno()):
        echo "Erro de conexão: ".mysqli_connect_error();
    endif;

    